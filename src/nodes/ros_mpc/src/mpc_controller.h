#ifndef SRC_MPC_CONTROLLER_H
#define SRC_MPC_CONTROLLER_H
#include <vector>
#include <cppad/cppad.hpp>              //第三方库cppad，用来实现自动微分的
#include <cppad/ipopt/solve.hpp>        //mpc的求解器ipopt
#include "eigen3/Eigen/Dense"           //第三方线性代数库eigen
using namespace std;
using CppAD::AD;
class MPC{
public:
    MPC();
    ~MPC();
    /**@descibe:为ipopt求解器提供好变量和约束
     * @param:state:state是初始状态向量[x,y,phi,v,cte,ephi],其中，phi是车辆的航向角，cte(cross-track error是车辆的横向
     *            偏差[m],ephi车辆的航向偏差[deg]。
     *       coeffs:是规划路径拟合多项式的系数
     * @return:车辆的第一次动作(方向盘转角和加速度)*/
    vector<double> Solve(Eigen::VectorXd state, Eigen::VectorXd coeffs);
};

class FG_eval {
    /**Fitted polynomial coefficients*/
public:
    typedef CPPAD_TESTVECTOR(AD<double>) ADvector;
    FG_eval(Eigen::VectorXd coeffs);
    /**@descibe:cost function（优化目标函数）
     * @param:fg:是模型约束的向量
     *        vars:是模型变量的向量(state & actuators)
     *               vars = [x, y, phi, v, cte, ephi, delta, a]*/
    void operator()(ADvector &fg, const ADvector &vars);
public:
    Eigen::VectorXd coeffs_;         //路径多项式的拟合系数
};

#endif //SRC_MPC_CONTROLLER_H
