### 更新日志

2021/03/10	增加后轮反馈控制算法rear wheel feedback control

2021/03/12    新增的后轮反馈算法调试效果较差，等待以后改进

2021/03/15    修改了ros_stanley节点的软件架构，去掉了Eigen/Dense包，代码可读性更好

2021/03/16	给stanley算法计算的车论转角增加一阶低通滤波模块，使得转角的输出曲线更为平滑

2021/05/18    新增ros_mpc节点，里面添加了MPC控制算法

2021/05/19    修改了ros_mpc节点的cmakelist.txt，增加了matplotlib-cpp绘图库

2021/09/17    修改了stanley的代码结构和readme

### 使用说明

#### 0分支说明

master分支：运行在ros端的控制算法，包括stanley,mpc。

PreScan分支：包含了prescan工程的压缩文件，解压后需要在matlab中添加自定义的ros msg，也就是src/nodes/msgs

配合控制程序，可以直接进行prescan-ros仿真。

#### 1环境说明

PC1:装win10系统；运行prescan2019.2和matlab2019b；ip = 172.16.6.248

PC2:Ubuntu18.04，ros melodic；ip = 172.16.6.70

p.s. PC1和PC2应该处于同一网段下

#### 2PC2(ubuntu18.04)操作说明

clone master分支的代码后

##### 2.1修改PC2的.bashrc文件，向其中添加PC2的ip地址

```
sudo gedit ~/.bashrc	#打开bashrc
#在bashrc文件的末尾添加如下两行
export ROS_IP=172.16.6.70	#根据自己的电脑ip做相应的改变  
export ROS_MASTER_URI=http://172.16.6.70:11311
```

##### 2.2运行ros master节点和ros_stanley节点

```
#运行ros master节点
roscore 
#进入ros的工作空间
source devel/setup.bash
rosrun ros_stanley n_stanley_controller	#运行ros_stanley节点
```

#### 3PC1(win10)操作说明

clone PreScan分支后，解压，用PreScan启动matlab，然后在matlab命令行中输入

```matlab
setenv('ROS_MASTER_URI', 'http://172.16.6.70:11311')	%pc2的ip
setenv('ROS_IP','172.16.6.248')		%PC1 win10的ip
rosinit('172.16.6.70')	%pc2的ip
```

#### 4控制算法测试效果

##### 4.1Stanley算法

###### 4.1.1仿真效果动图

![stanley_simulation](images/stanley_simulation.gif)

###### 4.1.2仿真场景

![stanley_scene](images/stanley_scene.png)



###### 4.1.3规划轨迹和实际行驶轨迹偏差

1. 偏差总览

   ![stanley_track_deviation](images/stanley_track_deviation.png)

2. 横向偏差

   ![lateral_deviation](images/lateral_deviation.png)

###### 4.1.4方向盘转角随时间变化图

![steering_wheel_angle](images/steering_wheel_angle.png)



##### 4.2MPC算法

###### 4.2.1直线工况[N=25,dt=0.05,迭代60次]

![mpc_simulation](images/mpc_simulation.png)

